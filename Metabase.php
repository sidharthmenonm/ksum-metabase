<?php

namespace Ksum\Metabase;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class Metabase{
  public static function generate($resource, $params=[]){

    $metabaseSiteUrl = config('metabase.server');
    $metabaseSecretKey = config('metabase.secret');

    $config = Configuration::forSymmetricSigner(
      new Sha256(),
      InMemory::plainText($metabaseSecretKey)
    );

    $now = new \DateTimeImmutable();

    $token = $config->builder()
              ->issuedAt($now)
              ->expiresAt($now->modify('+1 hour'))
              ->withClaim('resource', $resource)
              ->withClaim('params', (object)$params)
              ->getToken($config->signer(), $config->signingKey());

    $token_string = $token->toString();

    $iframeUrl = "{$metabaseSiteUrl}/embed/dashboard/{$token_string}#bordered=true&titled=true";

    return $iframeUrl;
  }
}