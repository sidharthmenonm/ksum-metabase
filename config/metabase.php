<?php

return [

    'server' => env('METABASE_SERVER',null),
    'secret' => env('METABASE_SECRET',null),

];