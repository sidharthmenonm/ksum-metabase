<?php

namespace Ksum\Metabase;

use Illuminate\Support\ServiceProvider;

class MetabaseServiceProvider extends ServiceProvider{

    public function boot()
    {
      $this->publishes([
        __DIR__.'/config/metabase.php' => config_path('metabase.php'),
      ]);
    }
}